enum EOrderStatus {
    unconfirmed = 'unconfirmed',
    processing = 'processing',
    inTransit = 'in-transit',
    completed = 'completed',
    cancelled = 'cancelled',
}

export default EOrderStatus;
