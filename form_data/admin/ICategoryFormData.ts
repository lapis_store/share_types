interface ICategoryFormData {
    title: string;
    index: number;
    image?: string;
    parentId?: string;
}

export default ICategoryFormData;
