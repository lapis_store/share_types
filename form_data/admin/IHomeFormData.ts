import IBanner from "../../others/IBanner";

interface IHomeFormData {
    carousel: IBanner[],
    bannersRightCarousel: IBanner[],
    bannersBelowCarousel: IBanner[],
    brand: IBanner[],
}

export default IHomeFormData;