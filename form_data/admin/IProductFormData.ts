interface IProductFormData {
    slug: string;
    categoryId?: string;
    keyword: string;
    image: string;
    title: string;
    price: number;
    isPromotionalPrice?: boolean;
    promotionPrice?: number;
    promotionStartAt?: string;
    promotionEndAt?: string;
    summary: string;
}

export default IProductFormData;
