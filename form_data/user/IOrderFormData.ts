export interface IOrderItem {
    productId: string;
    quantity: number;
}

interface IOrderFormData {
    customerName: string;
    phoneNumber: string;
    address: string;
    email?: string;
    orderItems: IOrderItem[];
}

export default IOrderFormData;
