interface IBanner{
    image: string,
    url: string,
    alt: string
}

export default IBanner;