interface IMetaTag {
    name: string;
    content: string;
}

export default IMetaTag;
