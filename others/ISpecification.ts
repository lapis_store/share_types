// enum ESpecificationNames{
//     screen='Màn hình'
// }

interface ISpecification {
    name: string;
    value: string;
}

export default ISpecification;
