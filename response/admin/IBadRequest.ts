interface IBadRequest {
    message?: string;
}

export default IBadRequest;
