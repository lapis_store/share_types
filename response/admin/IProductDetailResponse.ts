import IMetaTag from '../../others/IMetaTag';
import ISpecification from '../../others/ISpecification';

interface IProductDetailResponse {
    _id: string;
    metaTag?: IMetaTag[];
    images: string[];
    information: string;
    promotion: string;
    promotionMore?: string;
    specifications: ISpecification[];
    description: string;
    createdAt: string;
    updatedAt: string;
}

export default IProductDetailResponse;
