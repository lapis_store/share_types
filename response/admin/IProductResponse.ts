interface IProductResponse {
    _id: string;
    slug: string;
    categoryId?: string;
    keyword: string;
    title: string;
    price: number;
    isPromotionalPrice: boolean;
    promotionPrice: number;
    promotionStartAt?: string;
    promotionEndAt?: string;
    summary: string;
    image: string;
    createdAt: string;
    updatedAt: string;
}

export default IProductResponse;
