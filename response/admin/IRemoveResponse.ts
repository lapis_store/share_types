interface IRemoveResponse {
    _id: string;
}

export default IRemoveResponse;
