interface ICategoryResponse {
    _id: string;
    index: number;
    title: string;
    image?: string;
    parentId?: string;
    createdAt: string;
    updatedAt: string;
}

export default ICategoryResponse;
