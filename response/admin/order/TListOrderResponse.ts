import EOrderStatus from '../../../database/EOrderStatus';

export interface IOrderItemResponse {
    productId: string;
    productName: string;
    price: number;
    quantity: number;
}

interface IOrderResponse {
    _id: string;
    customerName: string;
    address: string;
    phoneNumber: string;
    total: number;
    orderStatus: EOrderStatus;
    updatedAt: string;
    createdAt: string;
    orderItems: IOrderItemResponse[];
}

export default IOrderResponse;
