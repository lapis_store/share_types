import IBanner from "../../others/IBanner";


interface IHomeResponse {
    carousel: IBanner[],
    bannersRightCarousel: IBanner[],
    bannersBelowCarousel: IBanner[],
    brand: IBanner[],
}

export default IHomeResponse;