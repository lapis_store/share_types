interface IAdminLoginResponse {
    status: 'successfully' | 'failure';
    accessToken?: string;
    refreshToken?: string;
}

export default IAdminLoginResponse;
