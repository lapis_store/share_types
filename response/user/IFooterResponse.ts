export default interface IFooterResponse {
    title?: string;
    align?: 'center' | 'left' | 'right';
    width: 'f100' | 'f75' | 'f50' | 'f25';
    description?: string;
    links?: { title: string; url: string }[];
    icons?: { name: string; url: string }[];
}
