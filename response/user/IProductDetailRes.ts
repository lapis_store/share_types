import IMetaTag from '../../others/IMetaTag';
import ISpecification from '../../others/ISpecification';

interface IProductDetailRes {
    _id?: string;
    metaTag?: IMetaTag[];
    images?: string[];
    information?: string;
    promotion?: string;
    promotionMore?: string;
    specifications?: ISpecification[];
    description?: string;
}

export default IProductDetailRes;
