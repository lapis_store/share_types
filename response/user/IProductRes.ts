interface IProductRes {
    _id: string;
    slug: string;
    image: string;
    title: string;
    price: number;
    isPromotion: boolean;
    promotionPrice?: number;
    summary: string;
}

export default IProductRes;
