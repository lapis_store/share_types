interface ICategoryRes {
    _id: string;
    index: number;
    title: string;
    image?: string;
    parentId?: string;
    createdAt: string;
}

export default ICategoryRes;
