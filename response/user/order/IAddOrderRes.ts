interface IAddOrderRes {
    _id: string;
    customerName: string;
    address: string;
    phoneNumber: string;
    total: number;
    orderItems: {
        productId: string;
        productName: string;
        price: number;
        quantity: number;
    }[];
}

export default IAddOrderRes;
